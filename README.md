#Dynamic DNS (for Cloudflare)

This PHP script rewraps Cloudflare's API endpoint to update DNS records.

This can be useful when your DDNS client does not support sending additional auth headers as required by Cloudflare's API.

An example for this is Synology's DSM OS which is only able to send simple GET requests to update the IP.

##Requirements
 - Cloudflare Global API Key
 - Cloudflare Account Email
 - DNS Zone (Zone ID; get it from [https://dash.cloudflare.com/](https://www.cloudflare.com/a/overview/))
 - DNS Identifier: get it by running `curl "https://api.cloudflare.com/client/v4/zones/<cloudflare-zone-id>/dns_records?name=<domain>" --silent -X GET -H "X-Auth-Email: <cloudflare-account-email>" -H "X-Auth-Key: <cloudflare-api-key>" -H "Content-Type: application/json"`
 
##Usage
Send a GET request to `https://ddns.flx.link/update.php?domain=<domain>&ip=<OPTIONAL user ip>&cf_key=<cloudflare api key>&cf_email=<cloudflare account email>&cf_dnszone=<dns zone>&cf_identifier=<dns identifier>`.

###For Synology's DSM:

Service provider: DDNS_for_Cloudflare
Query URL: `https://ddns.flx.link/update.php?dsm&domain=__HOSTNAME__&cf_key=__PASSWORD__&cf_email=__USERNAME__&ip=__MYIP__&cf_dnszone=dsm&cf_identifier=dsm`

Hostname: `<Domain>`

Username/Email: `<Cloudflare Email>---<Zone ID>---<DNS Identifier>`

Password/Key: `<Cloudflare API Key>`

###Response
The script responds with a JSON response:
`{"success":true}` or `{"success":false,"errors":["Missing required argument: cf_identifier"]}`