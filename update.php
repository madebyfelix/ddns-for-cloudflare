<?php
/**
 * Created by PhpStorm.
 * User: Felix
 * Date: 16.10.2018
 * Time: 23:36
 */

header("Content-Type: application/json");

$errors = array();

if (!isset($_GET["domain"])) {
    array_push($errors, "Missing required argument: domain");
}
if (!isset($_GET["cf_key"])) {
    array_push($errors, "Missing required argument: cf_key");
}
if (!isset($_GET["cf_email"])) {
    array_push($errors, "Missing required argument: cf_email");
}
if (!isset($_GET["cf_dnszone"])) {
    array_push($errors, "Missing required argument: cf_dnszone");
}
if (!isset($_GET["cf_identifier"])) {
    array_push($errors, "Missing required argument: cf_identifier");
}

if (isset($_GET["dsm"])) {

    if (count(explode("---", $_GET["cf_email"])) == 3) {
        $input = $_GET["cf_email"];
        $_GET["cf_email"] = explode("---", $input)[0];
        $_GET["cf_dnszone"] = explode("---", $input)[1];
        $_GET["cf_identifier"] = explode("---", $input)[2];
    } else {
        array_push($errors, "DSM mode: The argument cf_email must be in format Domain---Zone_ID---DNS_Identifier");
    }

}

if (count($errors) > 0) {
    die(json_encode(array("success" => false, "errors" => $errors)));
}

$ip = isset($_SERVER["HTTP_CF_CONNECTING_IP"]) ? $_SERVER["HTTP_CF_CONNECTING_IP"] : $_SERVER["REMOTE_ADDR"];
if (isset($_GET["ip"])) {
    $ip = $_GET["ip"];
}

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://api.cloudflare.com/client/v4/zones/" . $_GET["cf_dnszone"] . "/dns_records/" . $_GET["cf_identifier"]);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("type" => "A", "name" => $_GET["domain"], "content" => $ip, "ttl" => 1, "proxied" => false)));
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

$headers = array("Content-Type: application/json", "X-Auth-Email: " . $_GET["cf_email"], "X-Auth-Key: " . $_GET["cf_key"]);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    array_push($errors, "Error performing request: " . curl_error($ch));
}
curl_close($ch);

if (count($errors) > 0) {
    die(json_encode(array("success" => false, "errors" => $errors)));
} else {

    $response = json_decode($result, true);

    if ($response["success"] == true) {
        echo json_encode(array("success" => true));
    } else {
        array_push($errors, "Request failed. Cloudflare API response: " . $result);
        die(json_encode(array("success" => false, "errors" => $errors)));
    }

}